import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
       
       double zuzahlenderBetrag; 
       double eingezahlterGesamtbetrag;
       double eingeworfeneM�nze;
       double r�ckgabebetrag;
       byte anzahlfahrkarten;

       //Grundeingaben werden get�tigt
       System.out.print("Zu zahlender Betrag (EURO): ");
       zuzahlenderBetrag = tastatur.nextDouble();
       System.out.print("Anzahl der Fahrkarten: ");
       anzahlfahrkarten = tastatur.nextByte();
       
       //Anzahl der Fahrkarten und der zu zahlende Betrag werden multipliziert
       // -----------------
       zuzahlenderBetrag = zuzahlenderBetrag * anzahlfahrkarten;

       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = 0.0;
       while(eingezahlterGesamtbetrag < zuzahlenderBetrag)
       {
    	   System.out.printf("Noch zu zahlen: %.2f\n" , (zuzahlenderBetrag - eingezahlterGesamtbetrag));
    	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
    	   eingeworfeneM�nze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneM�nze;
       }

       // Fahrscheinausgabe
       // -----------------
       System.out.println("\nFahrschein/e werden ausgegeben");
       for (double i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");

       // R�ckgeldberechnung und -ausgabe
       // -------------------------------
       r�ckgabebetrag = eingezahlterGesamtbetrag - zuzahlenderBetrag;
       if(r�ckgabebetrag > 0.0)
       {
    	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f EURO " , r�ckgabebetrag);
    	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

           while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
           {
        	  System.out.println("2 EURO");
	          r�ckgabebetrag -= 2.0;
           }
           while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
           {
        	  System.out.println("1 EURO");
	          r�ckgabebetrag -= 1.0;
           }
           while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
           {
        	  System.out.println("50 CENT");
	          r�ckgabebetrag -= 0.5;
           }
           while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
           {
        	  System.out.println("20 CENT");
 	          r�ckgabebetrag -= 0.2;
           }
           while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
           {
        	  System.out.println("10 CENT");
	          r�ckgabebetrag -= 0.1;
           }
           while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
           {
        	  System.out.println("5 CENT");
 	          r�ckgabebetrag -= 0.05;
           }
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein/die Fahrscheine\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir w�nschen Ihnen eine gute Fahrt.");
    }
}

//Begr�nden Sie Ihre Entscheidung f�r die Wahl des Datentyps!

//Den Datentyp Byte habe ich verwendet, da es sich nur um relativ kleine ganzzahlige Variablen handeln sollte.
//Den Datentyp Double verwendete ich, um �Doppelt genaue� Gleitkommazahlen darstellen zu k�nnen.


//Erl�utern Sie detailliert, was bei der Berechnung des Ausdrucks anzahl * einzelpreis passiert.

//Anzahl der Fahrkarten und der zu zahlende Betrag werden multipliziert, so dass der noch zu zahlende Preis ausgegeben wird.
//�ber den Quellcode errechnet das System den R�ckgabebetrag und zahlt diesen aus.