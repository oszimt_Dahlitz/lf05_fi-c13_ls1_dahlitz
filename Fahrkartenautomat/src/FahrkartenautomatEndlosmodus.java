	import java.util.Scanner;

	public class FahrkartenautomatEndlosmodus {

		public static void main(String[] args) {

//Variablen
			double zuZahlenderBetrag = 0;
			double rueckgabebetrag;

// nach dem Zu zahlenden Betrag wir die Anzahl der Tickets eingegeben
// die Anzahl der Karten und der Zu zahlende Betrag wird multipliziert

			zuZahlenderBetrag= fahrkartenbestellungErfassen(zuZahlenderBetrag);

			rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);

			fahrkartenAusgeben();
			rueckgeldAusgeben(rueckgabebetrag);

// Geldeinwurf
// -----------

// Fahrscheinausgabe
// -----------------


// R�ckgeldberechnung und -Ausgabe
// -------------------------------


			System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
					"vor Fahrtantritt entwerten zu lassen!\n"+
					"Wir w�nschen Ihnen eine gute Fahrt.");
		}


//Methodenaufrufe

		public static double fahrkartenbestellungErfassen(double zuzahlenderBetrag) {
			Scanner tastatur = new Scanner(System.in);

			byte anzahlKarten;
			int eingabe = 0;
			boolean pruefe = true;

			while (pruefe) {

				System.out.println("W�hlen Sie ihre Wunschfahrkarte f�r Berlin AB aus:\r\n"
						+ " Einzelfahrschein Regeltarif AB [2,90 EUR] (1)\r\n"
						+ " Tageskarte Regeltarif AB [8,60 EUR] (2)\r\n"
						+ " Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)");
				System.out.println("");
				System.out.println("Gib Zahl 1,2 oder 3 ein: ");
				System.out.println("");
				eingabe = tastatur.nextInt();

				if (eingabe == 1) {
					System.out.println("Sie m�ssen 2,90 bezahlen: ");
					zuzahlenderBetrag = 2.90;
					System.out.println("Wie viele Fahrkarten wollen sie?: ");
					anzahlKarten = tastatur.nextByte();
					zuzahlenderBetrag *= anzahlKarten;
					pruefe = false;
				}

				else if (eingabe == 2) {
					System.out.println("Sie m�ssen 8,60 bezahlen: ");
					zuzahlenderBetrag = 8.60;
					System.out.println("Wie viele Fahrkarten wollen sie?: ");
					anzahlKarten = tastatur.nextByte();
					zuzahlenderBetrag *= anzahlKarten;
					pruefe = false;
				}

				else if (eingabe == 3) {
					System.out.println("Sie m�ssen 23,50 bezahlen: ");
					zuzahlenderBetrag = 23.50;
					System.out.println("Wie viele Fahrkarten wollen sie?: ");
					anzahlKarten = tastatur.nextByte();
					zuzahlenderBetrag *= anzahlKarten;
					pruefe = false;
				}

				else {
					System.out.println("Flasche Eingabe.");
					System.out.println("");
				}
			}
			return zuzahlenderBetrag;
		}

		public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
			Scanner tastatur = new Scanner(System.in);

			double rueckgabebetrag = 0;
			double eingezahlterGesamtbetrag = 0.00;
			double eingeworfeneM�nze;

			while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
			{
				System.out.printf("Noch zu zahlen: %.2f Euro \n " , (zuZahlenderBetrag - eingezahlterGesamtbetrag));
				System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
				eingeworfeneM�nze = tastatur.nextDouble();
				eingezahlterGesamtbetrag += eingeworfeneM�nze;
				rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
			}

			return rueckgabebetrag;
		}

		public static void fahrkartenAusgeben() {

			System.out.println("\nFahrschein wird ausgegeben");
			for (int i = 0; i < 8; i++)
			{
				System.out.print("=");
				try {
					Thread.sleep(250);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			System.out.println("\n\n");
		}

		public static double rueckgeldAusgeben(double rueckgabebetrag) {

			double zuZahlenderBetrag = 0;

			rueckgabebetrag = rueckgabebetrag - zuZahlenderBetrag;
			if(rueckgabebetrag > 0.00)
			{
				System.out.printf("Der R�ckgabebetrag in H�he von %.2f Euro \n" , rueckgabebetrag );
				System.out.println("wird in folgenden M�nzen ausgezahlt:");

				while(rueckgabebetrag >= 2.00) // 2 EURO-M�nzen
				{
					System.out.println("2 EURO");
					rueckgabebetrag -= 2.00;
				}
				while(rueckgabebetrag >= 1.00) // 1 EURO-M�nzen
				{
					System.out.println("1 EURO");
					rueckgabebetrag -= 1.00;
				}
				while(rueckgabebetrag >= 0.50) // 50 CENT-M�nzen
				{
					System.out.println("0,50 CENT");
					rueckgabebetrag -= 0.50;
				}
				while(rueckgabebetrag >= 0.20) // 20 CENT-M�nzen
				{
					System.out.println("0,20 CENT");
					rueckgabebetrag -= 0.20;
				}
				while(rueckgabebetrag >= 0.10) // 10 CENT-M�nzen
				{
					System.out.println("0,10 CENT");
					rueckgabebetrag -= 0.10;
				}
				while(rueckgabebetrag >= 0.05)// 5 CENT-M�nzen
				{
					System.out.println("0,05 CENT");
					rueckgabebetrag -= 0.05;
				}
			}
			return rueckgabebetrag;

		}
}