import java.util.Scanner;

class FahrkartenautomatneuA33{
	public static double fahrkartenbestellungErfassen() {
		Scanner tastatur = new Scanner(System.in);
		System.out.print("Zu zahlender Betrag (EURO): ");
	       double zuzahlenderBetrag = tastatur.nextDouble(); 
	       System.out.print("Anzahl der Fahrkarten: ");
	       byte anzahlfahrkarten = tastatur.nextByte();
	       zuzahlenderBetrag = zuzahlenderBetrag * anzahlfahrkarten;
	       return zuzahlenderBetrag;
	}
	public static double fahrkartenBezahlen(double zuzahlenderBetrag) {
		Scanner tastatur = new Scanner(System.in);
		double eingezahlterGesamtbetrag;
	    double eingeworfeneM�nze;
	    eingezahlterGesamtbetrag = 0.0;
		while(eingezahlterGesamtbetrag < zuzahlenderBetrag)
	       {
	    	   System.out.printf("Noch zu zahlen: %.2f\n" , (zuzahlenderBetrag - eingezahlterGesamtbetrag));
	    	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
	           System.out.println("\nVergessen Sie nicht, den Fahrschein/die Fahrscheine\n"+
                       "vor Fahrtantritt entwerten zu lassen!\n"+
                       "Wir w�nschen Ihnen eine gute Fahrt.");
	    	   eingeworfeneM�nze = tastatur.nextDouble();
	           eingezahlterGesamtbetrag += eingeworfeneM�nze;
	       } 
		return eingezahlterGesamtbetrag;
	}
	public static void fahrkartenAusgeben() {
		 System.out.println("\nFahrschein/e werden ausgegeben");
	       for (double i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       }
	       System.out.println("\n\n");
	}
	public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuzahlenderBetrag) {
		double r�ckgabebetrag;
		 r�ckgabebetrag = eingezahlterGesamtbetrag - zuzahlenderBetrag;
	       if(r�ckgabebetrag > 0.0)
	       {
	    	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f EURO " , r�ckgabebetrag);
	    	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

	           while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
	           {
	        	  System.out.println("2 EURO");
		          r�ckgabebetrag -= 2.0;
	           }
	           while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
	           {
	        	  System.out.println("1 EURO");
		          r�ckgabebetrag -= 1.0;
	           }
	           while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
	           {
	        	  System.out.println("50 CENT");
		          r�ckgabebetrag -= 0.5;
	           }
	           while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
	           {
	        	  System.out.println("20 CENT");
	 	          r�ckgabebetrag -= 0.2;
	           }
	           while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
	           {
	        	  System.out.println("10 CENT");
		          r�ckgabebetrag -= 0.1;
	           }
	           while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
	           {
	        	  System.out.println("5 CENT");
	 	          r�ckgabebetrag -= 0.05;
	           }
	       } 

	       System.out.println("\nVergessen Sie nicht, den Fahrschein/die Fahrscheine\n"+
	                          "vor Fahrtantritt entwerten zu lassen!\n"+
	                          "Wir w�nschen Ihnen eine gute Fahrt.");
	}
    public static void main(String[] args)
    {
       //Variablen
       double zuzahlenderBetrag; 
       double eingezahlterGesamtbetrag;
     

       zuzahlenderBetrag = fahrkartenbestellungErfassen();
       eingezahlterGesamtbetrag = fahrkartenBezahlen(zuzahlenderBetrag);
       fahrkartenAusgeben();
       rueckgeldAusgeben(zuzahlenderBetrag, eingezahlterGesamtbetrag);
       
     
  
    }
}

