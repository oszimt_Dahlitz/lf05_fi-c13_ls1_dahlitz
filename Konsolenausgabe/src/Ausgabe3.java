
public class Ausgabe3 {

	public static void main(String[] args) {
		
		System.out.printf( "%.5s\n", 22.4234234); 
		System.out.printf( "%.6s\n", 111.2222); 
		System.out.printf( "%.3s\n", 4.0); 
		System.out.printf( "%.10s\n", 1000000.551); 
		System.out.printf( "%.5s\n", 97.34);

	}

}
