
public class Aufgabe1dot5 {

	public static void main(String[] args) {
		
		System.out.print("Ich mag mein Auto.");
		System.out.println("Es ist ein Volvo V90.");
		
		System.out.print("Ich mag mein \"Auto\".\n");
		System.out.println("Es ist ein Volvo V90.");
		
		//Der Unterschied zwischen print() und println besteht darin, dass println() zus�tzlich einen Zeilenumbruch nach der Ausgabe einf�gt.
		
	}

}
