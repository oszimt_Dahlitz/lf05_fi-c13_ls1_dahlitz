
public class Ausgabe22 {

	public static void main(String[] args) {
		
		// n
		String a1 = "=";
		String a = "0!";
		String b = "1!";
		String c = "2!";
		String d = "3!";
		String e = "4!";
		String f = "5!";
		// Rechnung
			String g = "= 1";
			String h = "= 1 * 2";
			String i = "= 1 * 2 * 3";
			String j = "= 1 * 2 * 3 * 4";
			String k = "= 1 * 2 * 3 * 4 * 5";
		//Ergebnis
				String l = "=    1";
				String m = "=    2";
				String n = "=    6";
				String o = "=   24";
				String p = "=  120";
		System.out.printf( "%-5s", a ); System.out.printf( "%-21s", a1 ); System.out.printf( "%4s\n", l ); 
		System.out.printf( "%-5s", b ); System.out.printf( "%-21s", g ); System.out.printf( "%4s\n", l );
		System.out.printf( "%-5s", c ); System.out.printf( "%-21s", h ); System.out.printf( "%4s\n", m );
		System.out.printf( "%-5s", d ); System.out.printf( "%-21s", i ); System.out.printf( "%4s\n", n );
		System.out.printf( "%-5s", e ); System.out.printf( "%-21s", j ); System.out.printf( "%4s\n", o );
		System.out.printf( "%-5s", f ); System.out.printf( "%-21s", k ); System.out.printf( "%4s\n", p );
		
	}
}
