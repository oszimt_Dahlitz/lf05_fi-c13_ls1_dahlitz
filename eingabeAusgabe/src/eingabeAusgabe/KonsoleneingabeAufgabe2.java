package eingabeAusgabe;

import java.util.Scanner;

public class KonsoleneingabeAufgabe2 {

	public static void main(String[] args) // Hier startet das Programm 
	  { 
	    
		// Begr��ung
		System.out.println("Hallo lieber Nutzer,");
		
		//Frage nach dem Alter
		System.out.println("Wie geht es Ihnen?");
		
		//Frage nach dem Namen 
		System.out.println("Wie hei�en Sie?");
		
		//Frage nach dem Alter 
		System.out.println("Wie alt Sie?");
		
	    // Neues Scanner-Objekt myScanner wird erstellt     
	    Scanner myScanner = new Scanner(System.in);  
	     
	    System.out.print("Bitte geben Sie eine ganze Zahl ein: ");    
	     
	    // Die Variable zahl1 speichert die erste Eingabe 
	    double zahl1 = myScanner.nextDouble();  
	     
	    System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: "); 
	     
	    // Die Variable zahl2 speichert die zweite Eingabe 
	    double zahl2 = myScanner.nextDouble();  
	    
	    // Die Addition der Variablen zahl1 und zahl2  
	    // wird der Variable ergebnis zugewiesen. 
	    double ergebnis1 = zahl1 + zahl2; 
	    double ergebnis2 = zahl1 - zahl2;  
	    double ergebnis3 = zahl1 * zahl2;  
	    double ergebnis4 = zahl1 / zahl2;  
	     
	    System.out.print("\n\n\nErgebnis der Addition lautet: "); 
	    System.out.print(zahl1 + " + " + zahl2 + " = " + ergebnis1); 
	   
	    System.out.print("\n\n\nErgebnis der Subtraktion lautet: ");
	    System.out.print(zahl1 + " - " + zahl2 + " = " + ergebnis2);
	    
	    System.out.print("\n\n\nErgebnis der Multiplikation lautet: ");
	    System.out.print(zahl1 + " * " + zahl2 + " = " + ergebnis3);
	    
	    System.out.print("\n\n\nErgebnis der Division lautet: ");
	    System.out.print(zahl1 + " / " + zahl2 + " = " + ergebnis4);
	 
	    myScanner.close(); 
	     
	  }    
	}