import java.util.Scanner;

public class AufgabeQuadrat {

	public static void main(String[] args) {
		
		Scanner bdtastatur = new Scanner(System.in);
		
		System.out.println("Gebe eine zahl ein: ");
		int groesse = bdtastatur.nextInt();
		
		System.out.println("");
		
		for (int x = 0; x <= groesse -1; x++) {
			
			for (int y = 0; y <= groesse -1; y++) {
				
				if (x == 0 || x == groesse -1 || y == 0 || y == groesse -1) {
					System.out.printf("%2s", "*");
				} 
				else {
					System.out.print("  ");
				}
			}
			System.out.print("\n");
		}
	}

}
