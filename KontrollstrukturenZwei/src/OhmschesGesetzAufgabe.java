
	import java.util.Scanner;

	public class OhmschesGesetzAufgabe {
		
		public static void main(String[] args) {
		
			Scanner bdtastatur = new Scanner(System.in);
			char auswahl; 
			double u;
			double r;
			double i; 
			
			System.out.println("Was moechten Sie berechnen (U, I, R): ");
			auswahl = bdtastatur.next().charAt(0);
			
			if (auswahl == 'r') {
				System.out.println("Spannung in Volt eingeben: ");
				u = bdtastatur.nextDouble();
				System.out.println("Strom in Ampere eingeben: ");
				i = bdtastatur.nextDouble();
				r = u / i;
				System.out.printf("R = %.3f Ohm", r);
			}
			else if (auswahl == 'i') {
				System.out.println("Spannung in Volt eingeben: ");
				u = bdtastatur.nextDouble();
				System.out.println("Widerstand in Ohm eingeben: ");
				r = bdtastatur.nextDouble();
				i = u / r;
				System.out.printf("I = %.3f Ampere", i);
		}
			else if (auswahl == 'u') {
				System.out.println("Strom in Ampere eingeben: ");
				i = bdtastatur.nextDouble();
				System.out.println("Widerstand in Ohm eingeben: ");
				r = bdtastatur.nextDouble();
				u = i * r;
				System.out.printf("U = %.3f Volt", u);
			}
			}
	}
